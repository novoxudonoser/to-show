# encoding: utf-8
import copy
from core.data import validation
from core.meta.service import REGULAR
from core.suppliers import const
from core.data import ptc
from core import statuses as status

FLIGHT_TYPE_FROM = 'FROM'
FLIGHT_TYPE_TO = 'TO'

ROUTE_TYPE_RT = 'RT'
ROUTE_TYPE_OW = 'OW'
ROUTE_TYPES = {ROUTE_TYPE_OW, ROUTE_TYPE_RT}

LOCATION_AIRPORT = 'airport'

GROUP_ERRORS_PASS = {
    'All results filtered out',
    'No outward results found',
    'No return results found',
    'No results found',
    'Not available for the search',
    'Route not supported',
    'Results no longer available',
    'Timeout has been exceeded',
    'The supplier encountered an error'
}

PASSENGER_REQS = copy.copy(validation.FULL_PASSENGER_REQS)
PASSENGER_REQS.append('cardholder')

ROUTE_WAYS = [
    (FLIGHT_TYPE_TO, ('OutwardList', 'Outward')),
    (FLIGHT_TYPE_FROM, ('ReturnList', 'Return')),
]

MAP_SEATS_CLASSES = {
    'econom': 'Economy With Restrictions',
    'business': 'Business',
}

MAP_CONSISTS_AGES = {
    ptc.ADT: 30,
    ptc.CHD: 7,
    ptc.INF: 0,
}

MAP_GENDERS = {
    ptc.MALE: 'Mr',
    ptc.FEMALE: 'Mrs',
}

MAP_BOOLS = {
    'true': True,
    'false': False,
}

PARAM_GLOBAL = 'PARAM_GLOBAL'
PARAM_PER_PASSENGER = 'PARAM_PER_PASSENGER'

SUB_PARAMS_WHITELIST = {
    'DateOfBirth':            PARAM_PER_PASSENGER,
    'PassportExpiryDate':     PARAM_PER_PASSENGER,
    'PassportCountryOfIssue': PARAM_PER_PASSENGER,
    'Nationality':            PARAM_PER_PASSENGER,
    'PassportNumber':         PARAM_PER_PASSENGER,
    'LuggageOptions':         PARAM_PER_PASSENGER,
    'OutwardLuggageOptions':  PARAM_PER_PASSENGER,
    'ReturnLuggageOptions':   PARAM_PER_PASSENGER,
    'EndUserIPAddress':       PARAM_GLOBAL,
    'EndUserBrowserAgent':    PARAM_GLOBAL,
    'RequestOrigin':          PARAM_GLOBAL,
    'UserData':               PARAM_GLOBAL,
    'UseTFPrepay':            PARAM_GLOBAL,
    'FullCardNameBreakdown':  PARAM_GLOBAL,  # игнорируется т.к. мы не оплачиваем картой
    'PostCode':               PARAM_GLOBAL,
    'BillingAddress':         PARAM_GLOBAL,
    'CardSecurityNumber':     PARAM_GLOBAL,   # игнорируется т.к. мы не оплачиваем картой
    # 'ChildrenAndInfantsSearch': PARAM_GLOBAL, # в данной версии обработка параметров не происходит
    # 'ChildrenAndInfantsBooking': PARAM_GLOBAL, # в данной версии обработка параметров не происходит
    'DateOfBirthIsNotRequiredForAdults': PARAM_GLOBAL
}

MAP_PER_PASSENGER = {
    PARAM_PER_PASSENGER: True,
    PARAM_GLOBAL: False,
}

MAP_BOOK_STATUSES = {
    'Succeeded':             status.BOOKED,
    'BookingInProgress':     status.BOOKING,
    'Failed':                status.UNAVAIL,
    'Unconfirmed':           status.BOOKING,
    'UnconfirmedBySupplier': status.BOOKING
}

HTTP_HEADERS = {'Content-Type': 'text/xml; charset=utf-8'}
HTTP_METHOD = 'POST'

DATE_FORMAT = '%d/%m/%Y'
TIMESTAMP_FORMAT = '%d/%m/%Y-%H:%M'

SHAPE_DEFAULTS = {
    'baggage': {
        'hand': {'included': 1},
        'checked': {'included': 0},
    },
    'meal': {'included': True},
    'cancellation_policy': {'refundable': False},
    'aircraft': const.DEFAULT_AIRCRAFT_NAME,
    'subtype': REGULAR
}

