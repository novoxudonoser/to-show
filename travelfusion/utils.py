# encoding: utf-8
import logging
import decimal
import datetime

from requests import Response
import xmltodict
from tornado import gen

from core import utils
from core.fetch.common import make_request, requests_response_from_dict
from core.fetch.utils import async_fetch
from core.suppliers import common
from core.suppliers.common import log_book_error, send_bad_fetch

import const


logger = logging.getLogger(__name__)


def process_exception(settings, exception, metric):
    common.update_supplier_metric(settings.SUPPLIER_ID, metric)
    logger.warning(utils.ex2msg(exception))


@gen.coroutine
def call_api(settings, url, body, data, info=None):
    if info is None:
        info = {}
    info.update({
        'supplier': settings.SUPPLIER_ID,
    })
    request = make_request(
        url, 'POST',
        body=body,
        headers=const.HTTP_HEADERS,
        info=info)
    response = requests_response_from_dict((yield async_fetch(request)))

    data.update(status_code=response.status_code, response=response.text)
    if response.status_code != 200:
        log_book_error(settings.SUPPLIER_ID, 'invalid_status_code', response=response)
        raise gen.Return(None)

    raise gen.Return(response)


def proces_call_api_result(response, settings, tag, query_id=None):

    if isinstance(response, Response):
        response = {
            'status_code': response.status_code,
            'text': response.text,
        }

    success = False
    try:
        if response['status_code'] != 200:
            return None, response

        result = xmltodict.parse(response['text'])['CommandList']

        if result.get('CommandExecutionFailure') or result.get('DataValidationFailure') \
                or result[tag].get('@ecode'):  # TODO ALL ERRORS
            return None, response

        success = True
        return result[tag], response
    finally:
        if not success and query_id:
            send_bad_fetch(response, settings.SUPPLIER_ID, query_id)


def map_funcs(func_list, vars_list):
    return map(lambda (f, v): f(v) if f else v, zip(func_list, vars_list))


def extract_as_list(element):
    if not element:
        return []
    if isinstance(element, list):
        return element
    else:
        return [element]


def get_by_keypath(element, keypath):
    return reduce(lambda d, k: d.get(k, {}), keypath, element)


def extract_as_list_by_keypath(element, keypath):
    return extract_as_list(get_by_keypath(element, keypath))


def make_rates_index(currency_list, base_currency):
    from_usd_index = {}  # 1$ is n Y
    for record in currency_list:  # list of {Name: , Code: , UsdRate:}
        code = record['Code'].upper()
        rate = decimal.Decimal(record['UsdRate'])
        from_usd_index[code] = rate

    if base_currency == 'USD':
        return from_usd_index

    from_base_index = {}  # 1 base is n Y
    base_rate = from_usd_index[base_currency]
    for code, rate in from_usd_index.items():
        if code == 'USD':
            from_base_index[code] = decimal.Decimal('1.0') / base_rate
            continue

        if code == base_currency:
            from_base_index[base_currency] = decimal.Decimal('1.0')
            continue

        from_base_index[code] = rate / base_rate

    return from_base_index


_RATES = {}


def save_rates_index(rates):
    _RATES.update(rates)


def convert_to_base(value, currency, base_currency):
    if currency == base_currency:
        return (value, currency)
    logger.debug('converting %s to %s', currency, base_currency)
    if not _RATES:
        logger.error('rates index is empty')
        return (value, currency)

    rate = _RATES.get(currency)
    if not rate:
        logger.warning(
            'there is no travelfusion rates for pair %s / %s', base_currency, currency)
        return (value, currency)

    return (decimal.Decimal(value) / rate, base_currency)
