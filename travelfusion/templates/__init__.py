# encoding: utf-8

import os
import jinja2

from ..const import DATE_FORMAT


def make_date(t):
    return t.strftime(DATE_FORMAT)

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
templateEnv = jinja2.Environment(loader=jinja2.FileSystemLoader(THIS_DIR), trim_blocks=True, lstrip_blocks=True)

templateEnv.filters['date'] = make_date

GetSuppliers   = templateEnv.get_template('GetSuppliers.jinja2')
SupplierRoutes = templateEnv.get_template('SupplierRoutes.jinja2')
StartRouting   = templateEnv.get_template('StartRouting.jinja2')
CheckRouting   = templateEnv.get_template('CheckRouting.jinja2')
ProcessDetails = templateEnv.get_template('ProcessDetails.jinja2')
ProcessTerms   = templateEnv.get_template('ProcessTerms.jinja2')
StartBooking   = templateEnv.get_template('StartBooking.jinja2')
CheckBooking   = templateEnv.get_template('CheckBooking.jinja2')
GetCurrencies  = templateEnv.get_template('GetCurrencies.jinja2')
