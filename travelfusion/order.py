# encoding: utf-8
from collections import OrderedDict
import logging

import pycountry
from tornado import gen

from core import utils, statuses as status
from core.utils import first
from core.suppliers.common import build_price, get_matching_service, log_book_error
from core.data import ptc
from core import meta

import fetch
import const
import templates
from subparams import filter_by_subparams, log_filtred, parse_subparams
from utils import extract_as_list_by_keypath, call_api, proces_call_api_result

logger = logging.getLogger(__name__)


def make_passport_num(p):
    if p['passport_series']:
        return '{}{}'.format(p['passport_series'], p['passport_number'])
    else:
        return p['passport_number']


@gen.coroutine
def prebook_check(settings, fare, client_data, supplement, info=None):
    with utils.timings.log_duration('timing.%s.prebook_check' % settings.SUPPLIER_ID):
        if not client_data:
            if 'checked' in fare['identity']:
                raise gen.Return((status.AVAIL, fare))

            new_fare = first((yield fetch.check_fare(settings, fare, info=info)))
            if new_fare:
                new_fare['identity']['checked'] = True
                raise gen.Return((status.AVAIL, new_fare))

            raise gen.Return((status.UNAVAIL, None))

        service = get_matching_service(fare, supplement['package']['services'])
        if not service:
            raise gen.Return((status.INVALID, None))

        _, (depart_date, return_date) = meta.service.extract_transfer_params([service])

        to_id, from_id = (fare['identity']['ids'] + [None])[:2]
        routing_id = fare['identity']['routing_id']

        sub_params = OrderedDict([
            ('UserData', ', '.join([
                client_data['email'],
                client_data['phone'],
                client_data['cardholder']
            ]))
        ])

        for p in client_data['personal_data']:
            country_alpha2 = pycountry.countries.get(alpha_3=p['citizenship']).alpha_2
            age = ptc.age(p['birthday'], dt=(return_date or depart_date))
            p.update({
                'age': age,
                'gender': const.MAP_GENDERS[p['gender']],
                'passport_number': make_passport_num(p),
                'citizenship_alpha2': country_alpha2,
                'baggage_ids': fare['identity']['baggage_ids'] if ptc.detect_by_age(age) != ptc.INF else [],
                'sub_params': {}
            })

        ProcessTerms_request = templates.ProcessTerms.render(
            settings=settings, routing_id=routing_id, to_id=to_id, from_id=from_id,
            sub_params=sub_params, passengers=client_data,
        )

        req_data = {}
        ProcessTerms_response = yield call_api(
            settings, settings.BASE_URL, ProcessTerms_request, req_data, info=info)

        if not ProcessTerms_response:
            log_book_error(settings.SUPPLIER_ID, 'no_ProcessTerms_response')
            raise gen.Return((status.UNAVAIL, None))

        ProcessTerms_result, _ = proces_call_api_result(ProcessTerms_response, settings, 'ProcessTerms')

        if not ProcessTerms_result:
            log_book_error(settings.SUPPLIER_ID, 'ProcessTerms_error', response=ProcessTerms_response)
            raise gen.Return((status.UNAVAIL, None))

        # TODO check SupplierInfo UseTFPrepay
        subparams = parse_subparams(ProcessTerms_result['Router'])
        sub_params_filtered = {(fare['identity']['sub_supplier'], sub_param): 1
                               for sub_param in filter_by_subparams(subparams)}
        if sub_params_filtered:
            log_filtred(settings, sub_params_filtered, 'prebook_check')
            log_book_error(settings.SUPPLIER_ID, 'subparams_filtered', response=ProcessTerms_response)
            raise gen.Return((status.UNAVAIL, None))

        groups = extract_as_list_by_keypath(ProcessTerms_result, ['Router', 'GroupList', 'Group'])
        if len(groups) != 1:
            log_book_error(settings.SUPPLIER_ID, 'groups_num_error', response=ProcessTerms_response)
            raise gen.Return((status.UNAVAIL, None))

        group = utils.first(groups)

        fare['identity']['booking_id'] = ProcessTerms_result['TFBookingReference']

        new_price = build_price(float(group['Price']['Amount']), group['Price']['Currency'])

        fare['identity']['expected_price'] = {
            'amount': group['Price']['Amount'],
            'currency': group['Price']['Currency']
        }
        fare['identity']['prebook_response'] = req_data

        if (fare['price']['native'] != new_price['native']) | (fare['price']['currency'] != new_price['currency']):
            fare['price'].update(new_price)

        raise gen.Return((status.AVAIL, fare))


@gen.coroutine
def book(settings, fare, client_data, supplement, info=None):
    with utils.timings.log_duration('timing.%s.book' % settings.SUPPLIER_ID):
        book_data = {}

        try:
            service = get_matching_service(fare, supplement['package']['services'])
            if not service:
                raise gen.Return((status.INVALID, None))

            if not fare['identity'].get('booking_id') or not fare['identity'].get('expected_price'):
                raise gen.Return((status.INVALID, None))

            book_data['booking_id'] = fare['identity']['booking_id']

            StartBooking_request = templates.StartBooking.render(
                token=settings.TOKEN, booking_id=book_data['booking_id'],
                price_amount=fare['identity']['expected_price']['amount'],
                price_currency=fare['identity']['expected_price']['currency'],
                sandbox=settings.SANDBOX)

        except gen.Return:
            raise
        except Exception as e:
            log_book_error(settings.SUPPLIER_ID, 'exception', exception=e)
            raise gen.Return((status.ERROR, book_data))

        try:
            response = yield call_api(
                settings, settings.BASE_URL, StartBooking_request,
                book_data, info=info)
            book_data['response'] = response.text
        except Exception as e:
            log_book_error(settings.SUPPLIER_ID, 'exception', exception=e)
        finally:
            raise gen.Return((status.BOOKING, book_data))


@gen.coroutine
def check_book_status(settings, fare, book_data, info=None):
    try:

        CheckBooking_request = templates.CheckBooking.render(
            token=settings.TOKEN, booking_id=book_data['booking_id'])

        CheckBooking_response = yield call_api(
            settings, settings.BASE_URL, CheckBooking_request, {}, info=info)

        if not CheckBooking_response:
            raise gen.Return((status.ERROR, None))

        CheckBooking_result, _ = proces_call_api_result(
            CheckBooking_response, settings, 'CheckBooking')

        if not CheckBooking_result:
            raise gen.Return((status.ERROR, None))

        if CheckBooking_result.get('CardVerificationRequired') == 'true':
            log_book_error(settings.SUPPLIER_ID, 'invalid_card_verification_required_param',
                           response=CheckBooking_response)
            raise gen.Return((status.SUCCESS, status.INVALID))

        book_status = CheckBooking_result['Status']

        try:
            raise gen.Return(
                (status.SUCCESS, const.MAP_BOOK_STATUSES[book_status]))
        except KeyError:
            log_book_error(settings.SUPPLIER_ID, 'invalid_status_code', response=CheckBooking_response)
            raise gen.Return((status.SUCCESS, status.INVALID))

    except gen.Return:
        raise
    except Exception as e:
        logger.exception(utils.ex2msg(e))
        raise gen.Return((status.ERROR, None))
