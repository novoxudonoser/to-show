# encoding: utf-8
import logging
from datetime import timedelta

from tornado import gen

from core.suppliers.flights import RegularsSupplier
from core.suppliers.supplier_interface import SupplierState
from core.utils.tornado import schedule_periodic_task

import fetch


logger = logging.getLogger(__name__)


class TravelfusionInterface(RegularsSupplier):
    @gen.coroutine
    def initialize(self):
        info = {
            'supplier': self.settings.SUPPLIER_ID,
            'entry': 'refresh_rates',
            'kind': 'query',
        }
        self.state = SupplierState.INITIALIZING
        try:
            yield fetch.refresh_rates(self.settings, info=info)
            self.setup_whitelist()
        except:
            logger.exception(
                'supplier %s initializing failure', self.settings.SUPPLIER_ID)
            self.state = SupplierState.ERROR
        else:
            interval = getattr(self.settings, 'RATES_UPDATE_SECONDS', 15 * 60)
            schedule_periodic_task(
                fetch.refresh_rates, self.settings, info=info,
                run_every=timedelta(seconds=interval), shared=False)
            self.state = SupplierState.READY
