# encoding: utf-8
import time
import logging

from tornado import gen
import xmltodict

from core import utils, meta, inventory
from core.data import ptc
from core.fetch.common import make_request
from core.fetch.utils import fetch_shaper, mk_fetch_id, async_fetch
from core.meta.service import extract_transfer_params
from core.suppliers import common
from core.suppliers.common import send_results_metric
from core.utils import on_failure
from core.utils.tornado import queue_producer, for_queue_item
from core.worker import queue

import shape
import const
import templates
import subparams
from utils import (
    extract_as_list, get_by_keypath,
    extract_as_list_by_keypath,
    call_api, proces_call_api_result,
    make_rates_index, save_rates_index)


logger = logging.getLogger(__name__)


def make_query_request(
        settings, src_iata, dst_iata, depart_date, return_date, consist, seats_class, partner,
        real_search=False, query_id=None, async=True, info=None, **kwargs):

    ages = []
    for age_ptc, num in zip((ptc.ADT, ptc.CHD, ptc.INF), consist):
        ages.extend([const.MAP_CONSISTS_AGES[age_ptc]] * num)

    bags = sum(consist[:2])  # младенцы без багажа

    body = templates.StartRouting.render(
        token=settings.TOKEN, src_iata=src_iata, dst_iata=dst_iata, depart_date=depart_date, return_date=return_date,
        changes=settings.CHANGES, hops=settings.MAX_HOPS, timeout=settings.QUERY_SUPPLIER_TIMEOUT,
        seats_class=const.MAP_SEATS_CLASSES[seats_class], travellers_ages=ages, bags=bags, real_search=real_search,
        ip=kwargs.get('ip') or '', useragent=kwargs.get('useragent') or '', requestorigin=kwargs.get('requestorigin') or ''
    )

    info.update(fetch_id=mk_fetch_id())
    request = make_request(
        settings.BASE_URL,
        method=const.HTTP_METHOD,
        body=body,
        headers=const.HTTP_HEADERS,
        info=info,
    )

    return request


@queue.task
def query(
        settings, src_iata, dst_iata, depart_date, return_date,
        consist, seats_class, partner,
        query_id=None, async=True, info=None, **kwargs
):

    def metric(*args, **kwargs):
        common.update_supplier_metric(settings.SUPPLIER_ID, *args, **kwargs)

    @queue_producer
    def streaming_query(q):
        with utils.timings.log_duration('timing.%s.fetch.query' % settings.SUPPLIER_ID,):

            session_request = make_query_request(
                settings, src_iata, dst_iata, depart_date, return_date, consist, seats_class,
                partner, query_id=query_id, async=async, info=info, **kwargs)

            try:
                session_response_raw = yield async_fetch(session_request)
                result, session_response = proces_call_api_result(
                    session_response_raw, settings, 'StartRouting', query_id=query_id)
                if not result:
                    metric('session.not_started')
                    return

                session_id = result['RoutingId']
            except Exception as e:
                logger.exception(utils.ex2msg(e))
                metric('session.exception')
                return

            try:
                try_num, errors, ready_groups_ids = -1, 0, set()
                deadline = time.time() + settings.STREAM_FETCH_TIMEOUT

                while time.time() < deadline:
                    yield gen.sleep(settings.STREAM_CHECK_INTERVAL)
                    try_num += 1

                    stream_results, stream_results_response = \
                        yield _get_stream_results(
                            settings, session_id, try_num + 1, partner, query_id, info=info)

                    if not stream_results:
                        errors += 1
                        if errors > settings.STREAM_MAX_ERRORS:
                            metric('session.too_many_errors')
                            break

                        continue

                    any_undone_routers, done_routers = False, []
                    new_routers = extract_as_list_by_keypath(stream_results, ['RouterList', 'Router'])

                    if not new_routers:
                        metric('session.empty_routers')
                        continue

                    for router in new_routers:
                        groups_error = get_by_keypath(router, ['GroupList', '@etext'])
                        if groups_error:
                            if groups_error not in const.GROUP_ERRORS_PASS:
                                metric('session.error', data={
                                    'sub_supplier': router.get('Supplier', ''),
                                    'error': groups_error,
                                })
                            continue

                        groups = extract_as_list_by_keypath(router, ['GroupList', 'Group'])
                        if not groups:
                            metric(
                                'session.empty_groups', data={'sub_supplier': router['Supplier']})
                            continue

                        if not const.MAP_BOOLS[router['Complete']]:
                            any_undone_routers = True

                        unused_groups_ids = {g['Id'] for g in groups} - ready_groups_ids
                        if unused_groups_ids:

                            router['GroupList']['Group'] = [g for g in groups if g['Id'] in unused_groups_ids]

                            done_routers.append(router)
                            ready_groups_ids.update(unused_groups_ids)

                        else:
                            metric('session.replay_results')

                    if done_routers:
                        result = {
                            'routers': done_routers,
                            'disalowed_pairs': stream_results.get('DisallowedPairingList', {})
                        }

                        res, filtered = _shape_response(
                            settings, src_iata, dst_iata, depart_date, return_date, consist, seats_class,
                            partner, stream_results_response, result, session_id, query_id,
                            'timing.%s.fetch.query' % settings.SUPPLIER_ID)

                        subparams.log_filtred(settings, filtered, 'shape.query')

                        yield q.put(res)

                    if not any_undone_routers:  # The job is done and I go out. Another boring day. I leave it all behind me now. So many worlds away.
                        break
                else:
                    metric('session.timeout')

            except Exception as e:
                logger.exception(utils.ex2msg(e))
                metric('session.exception')

    return streaming_query()


@gen.coroutine
def _get_stream_results(settings, session_id, try_num, partner, query_id, info=None):
    body = templates.CheckRouting.render(token=settings.TOKEN, routing_id=session_id)
    info.update(try_num=str(try_num), fetch_id=mk_fetch_id())
    request = make_request(
        settings.BASE_URL,
        method=const.HTTP_METHOD,
        body=body,
        headers=const.HTTP_HEADERS,
        info=info,
    )
    response = yield async_fetch(request)
    raise gen.Return(proces_call_api_result(response, settings, 'CheckRouting', query_id=query_id))


@queue.task
@gen.coroutine
def query_by_service(
        settings, service, consist, seats_class, partner,
        query_id=None, async=True, info=None, *args, **kwargs):

    with utils.timings.log_duration('timing.%s.fetch.service.prepare' % settings.SUPPLIER_ID):

        (src_iata, dst_iata), (depart_date, return_date) = extract_transfer_params([service])
        match = sorted(a['id'] for a in service['activities'])
        info.update(fetch_id=mk_fetch_id())

        query_queues = query(
            settings, src_iata, dst_iata, depart_date, return_date,
            consist, seats_class, partner,
            real_search=True, query_id=query_id, async=True, info=info, **kwargs)

        results = []

        @for_queue_item
        def iterate(result):
            for service, fare in result:
                local_match = sorted(a['id'] for a in service['activities'])
                if local_match == match:
                    results.append(fare)

        yield iterate(query_queues)

        raise gen.Return(results)


@on_failure([])
@gen.coroutine
def check_fare(settings, fare, info=None, query_id=None):
    with utils.timings.log_duration('timing.%s.fetch.fare' % settings.SUPPLIER_ID):

        seats_class = fare['details']['seats_class']
        consist = fare['details']['consist']
        partner = fare['channel']

        service = meta.service.get(fare['service'])
        if not service:
            raise gen.Return([])

        (src_iata, dst_iata), (depart_date, return_date) = extract_transfer_params([service])

        to_id, from_id = (fare['identity']['ids'] + [None])[:2]
        routing_id = fare['identity']['routing_id']

        request = templates.ProcessDetails.render(
            token=settings.TOKEN, routing_id=routing_id, to_id=to_id, from_id=from_id)

        response = yield call_api(settings, settings.BASE_URL, request, {}, info=info)
        api_result, _ = proces_call_api_result(response, settings, 'ProcessDetails')
        if not api_result:
            raise gen.Return([])

        result = {
            'routers': [api_result['Router']],
            'disalowed_pairs': {}
        }

        result_items, filtered = _shape_response(
            settings, src_iata, dst_iata, depart_date, return_date, consist,
            seats_class, partner, response, result, routing_id, query_id=query_id,
            timing_description='timing.%s.fetch.fare' % settings.SUPPLIER_ID)

        subparams.log_filtred(settings, filtered, 'fetch.fare')

        new_fare = utils.first([f for _, f in result_items])
        if new_fare:
            new_fare['identity']['checked'] = True

            selected_baggage = fare['details']['baggage']['checked']
            if selected_baggage['included']:
                for v in meta.fare.variants_iter(new_fare):
                    vb = v['details']['baggage']['checked']
                    if vb['included'] >= selected_baggage['included'] and vb['weight'] >= selected_baggage['weight']:
                        new_fare = meta.fare.switch_variant(new_fare, v.get('variant_id'))
                        break
                else:
                    raise gen.Return([])

            raise gen.Return([new_fare])

        raise gen.Return([])


@queue.task
@fetch_shaper
@on_failure([])
def shape_service_response(
        settings, activities_match, src_iata, dst_iata, depart_date, return_date,
        consist, seats_class, partner,
        response=None, result=None, routing_id=None, query_id=None, async=True, **kwargs
):
    with utils.timings.log_duration('timing.%s.shape.service' % settings.SUPPLIER_ID):
        services_and_fares, filtered = _shape_response(
            settings, src_iata, dst_iata, depart_date, return_date, consist,
            seats_class, partner, response, result, routing_id, query_id,
            'timing.%s.fetch.query' % settings.SUPPLIER_ID)

        subparams.log_filtred(settings, filtered, 'shape.service')
        matches = []
        for service, fare in services_and_fares:
            local_match = sorted(a['id'] for a in service['activities'])
            if local_match == activities_match:
                matches.append(fare)
        return matches


def _shape_response(
        settings, src_iata, dst_iata, depart_date, return_date, consist, seats_class, partner,
        response, result, routing_id, query_id, timing_description=None, **kwargs):

    route_locations = [src_iata, dst_iata]
    route_type = const.ROUTE_TYPE_RT if return_date else const.ROUTE_TYPE_OW

    items = res, _ = shape.items(
        settings, result, route_locations, route_type, depart_date,
        return_date, consist, seats_class, partner, routing_id,
        query_id=query_id)

    send_results_metric(settings.SUPPLIER_ID, query_id, len(res), response)
    return items


@gen.coroutine
def refresh_rates(settings, info=None):
    with utils.timings.log_duration('timing.%s.fetch.whitelist' % settings.SUPPLIER_ID):
        request = templates.GetCurrencies.render(token=settings.TOKEN)
        response = yield call_api(settings, settings.BASE_URL, request, {}, info=info)
        result, _ = proces_call_api_result(response, settings, 'GetCurrencies')
        currency_list = result['CurrencyList']['Currency']

        base_currency = getattr(settings, 'BASE_CURRENCY', 'EUR')
        rates = make_rates_index(currency_list, base_currency)  # EUR to X rates
        save_rates_index(rates)
        raise gen.Return(rates)


@gen.coroutine
def whitelist(settings, info=None):
    with utils.timings.log_duration('timing.%s.fetch.whitelist' % settings.SUPPLIER_ID):
        routes = set()

        suppliers_request = templates.GetSuppliers.render(token=settings.TOKEN)
        suppliers_response = yield call_api(settings, settings.BASE_URL, suppliers_request, {}, info=info)

        for supplier in extract_as_list(xmltodict.parse(suppliers_response.text)
                                        ['CommandList']['GetBranchSupplierList']['BranchSupplierList']['Supplier']):
            supplier_routes_request = templates.SupplierRoutes.render(token=settings.TOKEN, supplier=supplier)
            supplier_routes_response = yield call_api(settings, settings.BASE_URL, supplier_routes_request, {}, info=info)

            supplier_routes_raw = (xmltodict.parse(supplier_routes_response.text)
                                   ['CommandList']['ListSupplierRoutes']['RouteList']['CityRoutes'])

            if not supplier_routes_raw:
                continue

            for supplier_route in supplier_routes_raw.split('\n'):
                src, dst = supplier_route[:3], supplier_route[3:]
                routes.add((src, dst))

        raise gen.Return(
            inventory.routes.register(settings.SUPPLIER_ID, list(routes)))
