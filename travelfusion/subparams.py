# encoding: utf-8

import re
from collections import namedtuple
from core.suppliers import common
from core.suppliers.common import build_price
from utils import extract_as_list, extract_as_list_by_keypath, map_funcs

import const

# example 'Please Select Luggage Option: 1 (1 bags - 23Kg total - 31.00 EUR), 2 (2 bags - 46Kg total - 101.00 EUR), 3 (3 bags - 69Kg total - 171.00 EUR), 4 (2 bags - 20Kg+10Kg total - 120.00 GBP)'
BAGGAGE_REGEX = re.compile(
    r"(\d+) \((\d+) bags - ((?:\d+Kg\+?)+) (?:total )?- (\d+.\d+) ([A-Z]+)\)")
BAGGAGE_WEIGHS_REGEX = re.compile(r"(\d+)(?:Kg\+?)")

Subparam = namedtuple('Subparam', ['name', 'is_optional', 'per_passenger', 'type', 'text'])
BaggageVariant = namedtuple(
    'BaggageVariant', ['option', 'bags_num', 'bag_weight', 'price', 'price_converted', 'currency'])


def parse_subparams(router):
    result = {}
    for subparam in extract_as_list_by_keypath(router,
                                               ['RequiredParameterList', 'RequiredParameter']):
        name = subparam['Name']
        is_optional, per_passenger = map(
            lambda k: const.MAP_BOOLS[subparam[k]], ('IsOptional', 'PerPassenger'))
        result[name] = Subparam(
            name=name, is_optional=is_optional, per_passenger=per_passenger, type=subparam['Type'],
            text=subparam.get('DisplayText', ''))
    return result


def filter_by_subparams(subparams):
    filtred_subparams = []

    for s in subparams.itervalues():

        filtred = not s.is_optional and not \
            (s.name in const.SUB_PARAMS_WHITELIST and
             const.MAP_PER_PASSENGER[const.SUB_PARAMS_WHITELIST[s.name]] == s.per_passenger)

        if filtred:
            filtred_subparams.append(s.name)

    return filtred_subparams


def log_filtred(settings, filtered, method):
    for (sub_supplier, sub_param), num in filtered.iteritems():
        common.update_supplier_metric(
            settings.SUPPLIER_ID, '{}.filtered_by_subparams'.format(method), data={
                'sub_supplier': sub_supplier,
                'sub_param': sub_param,
                'count': num
            })


def parse_baggage_variants(supplier_id, baggage_subparam, convert_price=False):
    def parse_bags_weigh(bags_weighs):
        return map(int, re.findall(BAGGAGE_WEIGHS_REGEX, bags_weighs))

    raw_variants = BAGGAGE_REGEX.findall(baggage_subparam.text)
    # output example [('1', '3', '69Kg', '171.00', 'EUR'), ('2', '2', '20Kg+10Kg', '120.00', 'GBP')]

    bags_variants = {}
    for raw_variant in raw_variants:

        option, bags_num, bags_weighs, price, currency = map_funcs(
            [int, int, parse_bags_weigh, float, str], raw_variant)
        one_bag_weight = int(
            bags_weighs[0] / bags_num) if len(bags_weighs) == 1 else min(bags_weighs)

        if convert_price:
            if price:
                price_converted = build_price(price, currency, supplier=supplier_id)
            else:
                price_converted = {
                    'native': float(0),
                    'currency': currency,
                    'supplier': float(0),
                }
        else:
            price_converted = None

        bags_variants[price] = BaggageVariant(
            option, bags_num, one_bag_weight, price, price_converted, currency)

    return bags_variants
