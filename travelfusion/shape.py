# encoding: utf-8
from datetime import timedelta, datetime
import logging
import uuid
import copy
import decimal

from core.inventory import baggage as inventory_baggage
from core import meta, utils
from core.meta.service import TRANSFER
from core.inventory import airports, airlines
from core.suppliers import common
from core.suppliers.common import build_price_multi
import itertools
from collections import defaultdict, OrderedDict
from decimal import Decimal

from utils import extract_as_list, extract_as_list_by_keypath, process_exception
from utils import convert_to_base
from subparams import parse_subparams, filter_by_subparams, parse_baggage_variants

import const

logger = logging.getLogger(__name__)


def make_timestamp(t):
    return datetime.strptime(t, const.TIMESTAMP_FORMAT)


def make_segments(settings, leg, route_locations, way, depart_date, return_date):
    segments = []

    for s in extract_as_list_by_keypath(leg, ['SegmentList', 'Segment']):

        if s['Origin']['Type'] != const.LOCATION_AIRPORT or s['Destination']['Type'] != const.LOCATION_AIRPORT:
            common.update_supplier_metric(
                settings.SUPPLIER_ID, 'shape.bad_segments.filtered.bad_geotype')
            return None

        flight_number = s['FlightId']['Number']
        airline = airlines.iata(s['FlightId']['Code'][:2])

        depart = make_timestamp(s['DepartDate'])
        arrive = make_timestamp(s['ArriveDate'])

        depart_airport = airports.iata(s['Origin']['Code'])
        arrive_airport = airports.iata(s['Destination']['Code'])

        raw_duration = s.get('Duration')
        if raw_duration:
            duration = int(raw_duration)
        else:
            travel_time = common.calculate_flight_duration(
                depart_airport['iata'], depart, arrive_airport['iata'], arrive,
                log_extra=common.get_supplier_log_extra(settings)) or timedelta(0)
            duration = int(travel_time.total_seconds() // 60)

        segments.append({
            'id': meta.service.make_segment_id(airline['iata'], flight_number, depart),
            'flight': flight_number,
            'airline': airline,
            'depart': {
                'time': depart,
                'airport': depart_airport,
                'city': depart_airport['city_title'],
                'country': depart_airport['country_title'],
                'country_code': depart_airport['country_code']
            },
            'arrive': {
                'time': arrive,
                'airport': arrive_airport,
                'city': arrive_airport['city_title'],
                'country': arrive_airport['country_title'],
                'country_code': arrive_airport['country_code']
            },
            'travel_time': duration,
            'aircraft': const.SHAPE_DEFAULTS['aircraft'],
        })

    flight_locations = [
        segments[0]['depart']['airport']['metro'], segments[-1]['arrive']['airport']['metro']
    ]

    control_date = {const.FLIGHT_TYPE_TO: depart_date, const.FLIGHT_TYPE_FROM: return_date}[way]

    if segments[0]['depart']['time'].date() != control_date.date():
        common.update_supplier_metric(
            settings.SUPPLIER_ID, 'shape.bad_segments.filtered.bad_daterange')
        return None

    if route_locations != flight_locations:
        common.update_supplier_metric(
            settings.SUPPLIER_ID, 'shape.bad_segments.filtered.bad_locations')
        return None

    return segments


def make_activity(activity_segments):
    activities = []
    for segments in activity_segments:
        activities.append({
            'id': ','.join(s['id'] for s in segments),
            'segments': segments,
        })

    return activities


def make_fare(
        settings, service, group, legs, router, routing_id, baggage,
        baggage_prices, baggage_ids, consist, seats_class, channel_id,
        price_with_baggage=False, query_id=None):

    """ формирует предложение поставщика продать сервис по указанной цене
    :param baggage_ids:
    """

    ids = [l['Id'] for l in legs]
    prices = []

    for leg in legs:

        if price_with_baggage:
            if 'PriceWithLuggage' in leg:
                price_element = 'PriceWithLuggage'
            else:
                return None
        else:
            price_element = 'Price'

        if price_element in leg and 'Amount' in leg[price_element] and 'Currency' in leg[price_element]:
            if leg['Price'].get('PriceIncludesTax') != 'true':
                common.update_supplier_metric(settings.SUPPLIER_ID, 'shape.fare.leg.bad_price_tax')
                return None
            prices.append((float(leg[price_element]['Amount']), leg[price_element]['Currency']))

    if 'Price' in group and 'Amount' in group['Price']:
        if not prices and group['Price'].get('PriceIncludesTax') != 'true':
            common.update_supplier_metric(settings.SUPPLIER_ID, 'shape.fare.group.bad_price_tax')
            return None
        prices.append((decimal.Decimal(group['Price']['Amount']), group['Price']['Currency']))
    elif not prices:
        common.update_supplier_metric(settings.SUPPLIER_ID, 'shape.fare.no_prices')
        return None

    passengars_with_baggage = sum(consist[:2])
    for baggage_price, baggage_price_currency in baggage_prices:
        if baggage_price:
            prices.append(
                (Decimal(baggage_price) * passengars_with_baggage, baggage_price_currency))

    # convert native to base currency
    base_currency = getattr(settings, 'BASE_CURRENCY', 'EUR')
    prices = [convert_to_base(val, cur, base_currency) for val, cur in prices]
    price = build_price_multi(prices, supplier=settings.SUPPLIER_ID)

    details = {
        'consist': consist,
        'seats_class': seats_class,
        'meal': const.SHAPE_DEFAULTS['meal'],
        'baggage': baggage
    }

    fare_data = {
        'id': uuid.uuid4().hex[:8],
        'query_id': query_id or uuid.uuid1().hex,
        'supplier': settings.SUPPLIER_ID,
        'channel': channel_id,
        'service': service['id'],
        'details': details,
        'book_priority': 0,
        'check_book_status_priority': 40,
        'price': price,
        'payment_schedule': {
            'type': meta.fare.PRE_PAY,
            'due_date': utils.today(),
            'amount': price['supplier'],
        },
        'identity': {
            'ids': ids,
            'routing_id': routing_id,
            'sub_supplier': router['Supplier'],
            'baggage_ids': baggage_ids
        },
        'warranty': None,
        'state': 'unconfirmable',
        'cancellation_policy': const.SHAPE_DEFAULTS['cancellation_policy'],
        'splittable': False,
        'passenger_reqs': const.PASSENGER_REQS,
    }
    return fare_data


def iterate_over_flights(settings, sub_supplier, flights, route_type, disable_pairs):

    if route_type == const.ROUTE_TYPE_RT:
        pairs_disabled = 0
        for segments in itertools.product(flights[const.FLIGHT_TYPE_TO],
                                          flights[const.FLIGHT_TYPE_FROM]):
            result = _, (leg_to, leg_from) = zip(*segments)
            if (leg_to['Id'], leg_from['Id']) not in disable_pairs:
                yield result
            else:
                pairs_disabled += 1
        if pairs_disabled:
            common.update_supplier_metric(
                settings.SUPPLIER_ID, 'shape.disable_pairs', data={
                    'sub_supplier': sub_supplier,
                    'count': pairs_disabled
                })

    if route_type == const.ROUTE_TYPE_OW:
        for segment in flights[const.FLIGHT_TYPE_TO]:
            yield zip(segment)


def items(
        settings, result, route_locations, route_type, depart_date, return_date, consist,
        seats_class, partner, routing_id, query_id=None):

    if route_type not in const.ROUTE_TYPES:
        raise ValueError

    routers, disalowed_pairs = result['routers'], result['disalowed_pairs']

    items = []
    subtype = const.SHAPE_DEFAULTS['subtype']
    filtered_by_subparams = defaultdict(int)

    locations = {
        const.FLIGHT_TYPE_TO: route_locations,
        const.FLIGHT_TYPE_FROM: route_locations[::-1]
    }

    disable_pairs = {(p['OutwardId'], p['ReturnId'])
                     for p in extract_as_list(disalowed_pairs.get('DisallowedPairing'))}

    for router in routers:
        try:
            sub_supplier = router['Supplier']

            subparams = parse_subparams(router)
            sub_params_filtered = filter_by_subparams(subparams)

            if 'LuggageOptions' in subparams:
                if not subparams['LuggageOptions'].per_passenger:
                    common.update_supplier_metric(
                        settings.SUPPLIER_ID, 'shape.bad_router.filtered.bad_baggage_params')
                    continue
                baggage_variants = {
                    k: [v]
                    for k, v in parse_baggage_variants(
                        settings.SUPPLIER_ID, subparams['LuggageOptions']).iteritems()
                }

            elif 'OutwardLuggageOptions' in subparams or 'ReturnLuggageOptions' in subparams:
                if 'ReturnLuggageOptions' not in subparams or 'ReturnLuggageOptions' not in subparams:
                    common.update_supplier_metric(
                        settings.SUPPLIER_ID, 'shape.bad_router.filtered.bad_baggage_params')
                    continue

                if not subparams['OutwardLuggageOptions'].per_passenger or not subparams['ReturnLuggageOptions'].per_passenger:
                    common.update_supplier_metric(
                        settings.SUPPLIER_ID, 'shape.bad_router.filtered.bad_baggage_params')
                    continue

                variants = {}
                vars_to = parse_baggage_variants(
                    settings.SUPPLIER_ID, subparams['OutwardLuggageOptions'], convert_price=True)
                vars_from = parse_baggage_variants(
                    settings.SUPPLIER_ID, subparams['ReturnLuggageOptions'], convert_price=True)

                # полный перебор т.к. не всегда багаж с меньшим весом дешевле
                for bag_vars in itertools.product(vars_to.itervalues(), vars_from.itervalues()):

                    weight = min([bg.bag_weight for bg in bag_vars])
                    bags = min([bg.bags_num for bg in bag_vars])
                    price = sum([Decimal(bg.price_converted['native']) for bg in bag_vars])
                    price_converted = sum(
                        [Decimal(bg.price_converted['supplier']) for bg in bag_vars])

                    var_key = (bags, weight)
                    var_value = (price_converted, price, bag_vars)
                    if var_key in variants:
                        old_price, _, _ = variants[var_key]
                        if price_converted < old_price:
                            variants[var_key] = var_value
                    else:
                        variants[var_key] = var_value

                baggage_variants = {price: bag_vars for _, price, bag_vars in variants.itervalues()}

            else:
                baggage_variants = {}

            baggage_checked = OrderedDict()
            for price in sorted(baggage_variants.keys()):
                baggage_variant = baggage_variants[price]
                baggage_struct = {
                    'included': min([bg.bags_num for bg in baggage_variant]),
                    'weight': min([bg.bag_weight for bg in baggage_variant])
                }
                baggage_price = [(bg.price, bg.currency) for bg in baggage_variant]
                baggage_ids = [bg.option for bg in baggage_variant]

                baggage_checked[price] = (baggage_struct, baggage_price, baggage_ids)

            for group in extract_as_list_by_keypath(router, ['GroupList', 'Group']):

                flights_parsts = {const.FLIGHT_TYPE_TO: [], const.FLIGHT_TYPE_FROM: []}

                for way, group_keypath in const.ROUTE_WAYS:
                    segments_locations = locations[way]
                    for leg in extract_as_list_by_keypath(group, group_keypath):
                        try:
                            segments = make_segments(
                                settings, leg, segments_locations, way, depart_date, return_date)
                            if segments:
                                flights_parsts[way].append([segments, leg])
                        except Exception as e:
                            process_exception(settings, e, 'shape.bad_segments')

                flights = iterate_over_flights(
                    settings, sub_supplier, flights_parsts, route_type, disable_pairs)

                if sub_params_filtered:

                    filtered_segments_num = sum(1 for _ in flights)

                    for sub_param in sub_params_filtered:
                        filtered_by_subparams[(sub_supplier, sub_param)] += filtered_segments_num
                else:

                    for activity_segments, legs in flights:
                        try:
                            service = meta.service.make(
                                TRANSFER, subtype, make_activity(activity_segments))

                            base_baggage = inventory_baggage.get_default_baggage(service)
                            if baggage_checked:
                                free_baggage = baggage_checked.get(Decimal(0))
                                if free_baggage:
                                    baggage_struct, baggage_price, baggage_ids = free_baggage
                                    base_baggage['checked'] = baggage_struct
                                    used_baggage = baggage_ids
                                else:
                                    baggage_price, baggage_ids = [], []
                                    base_baggage['checked'] = {'included': 0}
                                    used_baggage = None

                                base_fare = make_fare(
                                    settings, service, group, legs, router, routing_id,
                                    base_baggage, baggage_price, baggage_ids, consist, seats_class,
                                    partner, query_id=query_id)

                                if base_fare:
                                    for baggage_variant in baggage_checked.itervalues():
                                        baggage_struct, baggage_price, baggage_ids = baggage_variant
                                        if baggage_ids == used_baggage:
                                            continue
                                        baggage = copy.deepcopy(base_baggage)
                                        baggage['checked'] = baggage_struct

                                        fare = make_fare(
                                            settings, service, group, legs, router, routing_id,
                                            baggage, baggage_price, baggage_ids, consist,
                                            seats_class, partner, query_id=query_id)

                                        if fare:
                                            meta.fare.add_variant(base_fare, fare)

                            else:
                                baggage_price, baggage_ids = [], []

                                no_baggage = copy.deepcopy(base_baggage)
                                no_baggage['checked'] = {'included': 0}
                                base_fare = make_fare(
                                    settings, service, group, legs, router, routing_id, no_baggage,
                                    baggage_price, baggage_ids, consist,
                                    seats_class, partner, query_id=query_id)

                                if base_fare:
                                    fare_with_baggage = make_fare(
                                        settings, service, group, legs, router, routing_id,
                                        base_baggage, baggage_price, baggage_ids, consist,
                                        seats_class, partner, price_with_baggage=True, query_id=query_id)
                                    if fare_with_baggage:
                                        meta.fare.add_variant(base_fare, fare_with_baggage)

                            items.append((service, base_fare))
                        except Exception as e:
                            process_exception(settings, e, 'shape.bad_activity')

        except Exception as e:
            process_exception(settings, e, 'shape.bad_router')
    return items, filtered_by_subparams
